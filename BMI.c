﻿/*UTF-8*/
#include"BMI.h"

int main(void)
{
  printf("您的身高（米）？\n");
  scanf("%lf",&Height);    
  printf("您的体重（公斤）？\n");
  scanf("%lf",&Weight);
  Height_Square = pow(Height,2);   
  BMI = Weight / Height_Square;
  if (Height > 0 && Weight > 0)
  {
    printf("您的BMI指数:%.2f。", BMI);
    if (BMI < 18.5)
    {
      printf("您偏瘦。");
    }
    else if (BMI <= 24.9)
    {
      printf("您的BMI指数正常。");
    }
    else
    {
      printf("您超重。");
    }
  }
  else
  {
    printf("输入的数值必须大于0。三秒后暂停。\n");
  }
  
  Sleep(3000);
  system("cls");
  system("pause");
  return 0;
}